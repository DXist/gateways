# coding: utf-8

"""SMS gateway handler API."""

import jsonschema
import requests.exceptions

from collections import namedtuple
from gateways.models import PHONE_REGEX, SMSSendLog


SendResult = namedtuple(
    'SendResult', ['phone', 'status', 'error_code', 'error_msg', 'exception'])


class BaseHandler:
    REQUEST_ERROR = -999999999999999
    DESERIALIZATION_ERROR = REQUEST_ERROR + 1
    VALIDATION_ERROR = REQUEST_ERROR + 2

    def __call__(self, **kwargs):
        raise NotImplementedError

    @classmethod
    def log_send_result(cls, result):
        SMSSendLog.objects.create(
            gateway=cls.__name__,
            phone=result.phone,
            status=result.status,
            error_code=result.error_code,
            error_msg=result.error_msg,
            exception=result.exception,
        )

    @classmethod
    def validate(cls, gateway_data):
        raise NotImplementedError

    @classmethod
    def send(cls, user_data):
        assert hasattr(cls, 'url'), 'url attribute is required for sending'
        raise NotImplementedError


class JsonSchemaHandler(BaseHandler):

    """Handler that validates gateway output by JsonSchema"""

    @classmethod
    def validate(cls, response):
        return jsonschema.validate(response, cls.get_schema())

    @classmethod
    def get_schema(cls):
        return {
            '$schema': 'http://json-schema.org/draft-04/schema#',
            'title': 'Gateway responses',

            'type': 'object',
            'oneOf': [
                # success definition
                {
                    'properties': {
                        'status': {'type': 'string', 'enum': ['ok']},
                        'phone': {'type': 'string', 'pattern': PHONE_REGEX},
                    },
                    'required': ['status', 'phone'],
                },
                # error definition
                {
                    'properties': {
                        'status': {'type': 'string', 'enum': ['error']},
                        'phone': {'type': 'string', 'pattern': PHONE_REGEX},
                        'error_code': {'type': 'number'},
                        'error_msg': {'type': 'string'},
                    },
                    'required': ['status', 'phone', 'error_code', 'error_msg'],
                },
            ]

        }

    @classmethod
    def send(cls, user_data):
        assert hasattr(cls, 'url')
        response, send_result = cls._send(user_data)

        if send_result is None:
            try:
                cls.validate(response)
            except jsonschema.exceptions.ValidationError as e:
                send_result = SendResult(
                    **{'status': 'error', 'phone': user_data['phone'],
                       'error_code': cls.VALIDATION_ERROR, 'error_msg': e,
                       'exception': e})
            else:
                send_result = SendResult(
                    **{'status': response['status'],
                       'phone': response['phone'],
                       'error_code': response.get('error_code', ''),
                       'error_msg': response.get('error_msg', ''),
                       'exception': ''})
        cls.log_send_result(send_result)
        return send_result

    @classmethod
    def _send(cls, user_data):
        try:
            r = requests.post(cls.url, data=user_data)
        except (requests.exceptions.RequestException, ValueError) as e:
            return None, SendResult(
                **{'status': 'error', 'phone': user_data['phone'],
                   'error_code': cls.REQUEST_ERROR, 'error_msg': e,
                   'exception': e})
        try:
            response = r.json()
        except ValueError as e:
            return None, SendResult(
                **{'status': 'error', 'phone': user_data['phone'],
                   'error_code': cls.DESERIALIZATION_ERROR, 'error_msg': e,
                   'exception': e})
        return response, None


class SMSCenterHandler(JsonSchemaHandler):
    url = 'http://smscenter'


class SMSTrafficHandler(JsonSchemaHandler):
    url = 'http://smstraffic'


def get_handler(handler_name):
    """Return handler class by name."""
    handler = None

    def visitor(klass):
        nonlocal handler
        if klass.__name__ == handler_name:
            handler = klass
            return False
        return True

    _visit(BaseHandler, visitor)
    assert handler is not None, 'Handler is not found'
    return handler


def _visit(klass, visitor):
    for k in klass.__subclasses__():
        if visitor(k) is False:
            break
        _visit(k, visitor)
