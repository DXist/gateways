# coding: utf-8

from django.core.validators import RegexValidator
from django.db import models


PHONE_REGEX = '^\+?\d{5,15}'


class SMSSendLog(models.Model):

    date_time = models.DateTimeField(auto_now_add=True)
    gateway = models.CharField(max_length=30)
    phone = models.CharField(validators=[RegexValidator(PHONE_REGEX)],
                             max_length=16)
    status = models.CharField(max_length=50)
    error_code = models.CharField(max_length=1024)
    error_msg = models.CharField(max_length=1024)
    exception = models.CharField(max_length=1024)
