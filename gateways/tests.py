# coding: utf-8

import jsonschema.exceptions
import requests.exceptions

from django.test import TestCase, SimpleTestCase
from gateways.models import SMSSendLog
from gateways.handlers import get_handler, JsonSchemaHandler, SendResult


class TestValidation(SimpleTestCase):
    def _validate(self, obj):
        jsonschema.validate(obj, JsonSchemaHandler.get_schema())

    def assertValid(self, obj):
        self._validate(obj)
        return True

    def assertInvalid(self, obj):
        with self.assertRaisesMessage(
                jsonschema.exceptions.ValidationError, 'is not valid'):
            self._validate(obj)

    def test_success_is_valid(self):
        return self.assertValid({'status': 'ok', 'phone': '+12345'})

    def test_invalid_phone(self):
        return self.assertInvalid({'status': 'ok', 'phone': '+1'})

    def test_incomplete_error_is_invalid(self):
        return self.assertInvalid({'status': 'error', 'phone': '+12345'})

    def test_error_is_valid(self):
        return self.assertValid(
            {'status': 'error', 'phone': '+12345', 'error_code': -3,
             'error_msg': 'сообщение об ошибке'})

    def test_invalid(self):
        return self.assertInvalid({'foo': 1})


class TestHandlers(TestCase):
    def setUp(self):
        self.send = JsonSchemaHandler._send
        self.addCleanup(self.unpatch)
        self.h = get_handler('SMSCenterHandler')

    def patch(self, obj, result):
        def dummy(cls, user_data):
            return (obj, result)
        JsonSchemaHandler._send = classmethod(dummy)

    def unpatch(self):
        JsonSchemaHandler._send = self.send

    def test_gateway_error_log(self):
        self.assertEqual(self.h.__name__, 'SMSCenterHandler')

        self.patch({'status': 'error', 'phone': '+12345', 'error_code': -3,
                    'error_msg': 'сообщение об ошибке'}, None)
        send_result = self.h.send({'phone': '+12345', 'msg': 'Сообщение'})
        self.assertEqual(send_result.error_msg, 'сообщение об ошибке')

        send_log = SMSSendLog.objects.get()
        self.assertEqual(send_log.error_msg, 'сообщение об ошибке')
        self.assertEqual(send_log.gateway, self.h.__name__)
        self.assertEqual(send_log.error_code, str(-3))

    def test_validation_error_log(self):
        self.patch({'status': 'error', 'phone': '+12345'}, None)
        send_result = self.h.send({'phone': '+12345', 'msg': 'Сообщение'})
        self.assertTrue(isinstance(send_result.error_msg,
                                   jsonschema.exceptions.ValidationError))

        send_log = SMSSendLog.objects.get()
        self.assertTrue('is not valid' in send_log.exception)
        self.assertEqual(send_log.error_code, str(self.h.VALIDATION_ERROR))

    def test_request_error_log(self):
        def dummy(cls, user_data):
            e = requests.exceptions.RequestException(request='dummy')
            return (None, SendResult(
                **{'status': 'error', 'phone': user_data['phone'],
                   'error_code': JsonSchemaHandler.REQUEST_ERROR,
                   'error_msg': e, 'exception': e}))

        JsonSchemaHandler._send = classmethod(dummy)
        send_result = self.h.send({'phone': '+12345', 'msg': 'Сообщение'})
        self.assertTrue(isinstance(send_result.error_msg,
                                   requests.exceptions.RequestException))

        send_log = SMSSendLog.objects.get()
        self.assertEqual(send_log.error_code, str(self.h.REQUEST_ERROR))
